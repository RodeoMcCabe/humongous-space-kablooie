
extends Node2D

const GLOBALS = preload("res://scripts/globals.gd")

onready var label = get_node("label")
onready var timer = get_node("timer")

var move_speed = 0 # gets set in set_value()

func _ready():
	set_process(true)

func _process(delta): # not called if parent is not the player
	set_pos(Vector2(get_pos().x, get_pos().y - (move_speed * delta))) # move up a little each frame

func set_value(value, is_combo = null):
	if value >= 1:
		label.set_text("+" + str(value)) # str() doesn't print sign for positive numbers
		set_color(GLOBALS.GREEN)
	else:
		label.set_text(str(value)) # str() prints sign for negative numbers
		set_color(GLOBALS.RED)

	if value > 0:
		move_speed = 25 + value / 2.0
	else:
		move_speed = -25 - value / 2.0

	if is_combo != null and is_combo:
		handle_combo(value)

func set_color(color):
	label.set("custom_colors/font_color", color)

func handle_combo(value):
	get_node("anim").play("combo")

	var scale = 1 + value / 150.0
	set_scale(Vector2(scale, scale))

	set_z(1)

func on_timer_timeout():
	queue_free()
