
extends StreamPlayer

const MENU = "res://sounds/menu.ogg"
const EARTH = "res://sounds/earth.ogg"
const MARS = "res://sounds/mars.ogg"
const MERCURY = "res://sounds/mercury.ogg"
const VENUS = "res://sounds/venus.ogg"
const BOSS_INTRO = "res://sounds/boss-intro.ogg"
const BOSS_MAIN = "res://sounds/boss-main.ogg"
const BOSS_DEFEATED = "res://sounds/boss-defeated.ogg"

var muted = false

func play(stream):
	set_stream(stream)
	if not muted:
		.play()

func menu():
	play(load(MENU))

func earth():
	play(load(EARTH))

func mars():
	play(load(MARS))

func mercury():
	play(load(MERCURY))

func venus():
	play(load(VENUS))

func boss():
	play(load(BOSS_INTRO))
	set_loop(false)
	connect("finished", self, "on_boss_intro_finished", [], CONNECT_ONESHOT)

func boss_defeated(next_song):
	play(load(BOSS_DEFEATED))
	set_loop(false)
	connect("finished", self, "on_boss_defeated_finished", [next_song], CONNECT_ONESHOT)

func on_boss_intro_finished():
	play(load(BOSS_MAIN))
	set_loop(true)

func on_boss_defeated_finished(next_song):
	play(load(next_song))
	set_loop(true)