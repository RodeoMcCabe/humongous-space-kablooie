
extends Node2D

onready var parent = get_parent()
onready var gun = get_node("gun")
onready var invincibility = get_node("invincibility")
onready var blinker = get_node("blinker")

export var move_speed = 350.0
export var rot_speed = 20.0
export var hull_armor = 3

signal damaged
signal hull_armor_changed
signal gun_heat_changed
signal gun_shooting
signal gun_overheating
signal gun_cooled
signal destroyed

func _ready():
	invincibility.set_active(false)

func damage(source):
	hull_armor -= 1
	emit_signal("damaged", source)
	emit_signal("hull_armor_changed", hull_armor)

	if hull_armor < 1:
		emit_signal("destroyed", source)
	else:
		start_invincible()

func start_invincible():
	invincibility.set_active(true)
	invincibility.start()
	blinker.start()

func is_invincible():
	return invincibility.is_active()

func on_invincibility_timeout():
	invincibility.set_active(false)
	blinker.stop()
	parent.set_hidden(false)

func on_blinker_timeout(): # toggle hidden
	if parent.is_hidden():
		parent.set_hidden(false)
	else:
		parent.set_hidden(true)

func on_gun_cooled():
	emit_signal("gun_cooled")

func on_gun_heat_changed(heat):
	emit_signal("gun_heat_changed", heat)

func on_gun_overheating():
	emit_signal("gun_overheating")

func on_gun_shooting(bullets, ammo):
	emit_signal("gun_shooting", bullets, ammo)