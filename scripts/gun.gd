extends Node2D

const GLOBALS = preload("res://scripts/globals.gd")
const BULLET = preload("res://objects/bullet.scn")

onready var grandparent = get_parent().get_parent() # the player or enemy object the gun belongs to
onready var timer = get_node("timer")

export var overheat = 1500 # value at which the gun overheats and can't shoot
export var firing_heat = 450 # amount of heat that firing the gun generates
export var cooling_rate = 10 # amount of heat the gun loses each frame

var heat = 0 # the current heat of the gun
var overheating = false # true if heat exceeds overheat until heat reaches 0 again. Can't fire gun while true.
var shoot_mode = GLOBALS.ShootMode.NORMAL
var ammo = GLOBALS.Ammo.MISSILE

signal shooting # emitted when gun is fired\
signal heat_changed # emitted whenever gun heat changes
signal overheating # emitted first frame that overheating = true
signal cooled # emitted first frame gun is no longer overheating

func _ready():
	set_fixed_process(true)

	timer.set_active(false) # Timer class does not set active to false based on whether it's counting down

func _fixed_process(delta):

	# check heat
	if not overheating && heat > overheat:
		overheating = true
		emit_signal("overheating")

	if heat > 0:
		heat -= cooling_rate
		emit_signal("heat_changed", heat)
	elif heat < 0 || overheating == true:
		heat = 0
		overheating = false
		emit_signal("cooled")

func shoot(pos = null, rot = null, vel = null):
	if not overheating && not timer.is_active():
		# increase heat
		if shoot_mode == GLOBALS.ShootMode.SCATTER_BEAM:
			shoot_scatter_beam(pos, rot, vel)
		else:
			var b = [spawn_bullet(pos, rot, vel)] # pass as array for game::on_gun_shooting()
			emit_signal("shooting", b, ammo)
		heat += firing_heat
		emit_signal("heat_changed", heat)

		timer.set_active(true) # delay shooting again for a little bit
		timer.start()

func shoot_scatter_beam(pos = null, rot = null, vel = null):
	var angle = 0.0

	if rot != null:
		angle = rot - PI / 20
	else:
		angle = grandparent.get_rot() - PI / 20

	var b = []
	for i in range(5):
		b.append(spawn_bullet(pos, angle, vel))
		angle += PI / 40

	emit_signal("shooting", b, ammo)

func shoot_spinner(pos = null, rot = null, vel = null):
	if not overheating and not timer.is_active():
		var b = []

		for i in range(3):
			b.append(spawn_bullet(pos, rot, vel))
			rot += (PI / 3.0) * 2

		emit_signal("shooting", b, ammo)

		heat += firing_heat
		emit_signal("heat_changed", heat)

		timer.set_active(true) # delay shooting again for a little bit
		timer.start()

func spawn_bullet(pos = null, rot = null, vel = null):
	var b = BULLET.instance()
	transform_bullet(b, pos, rot, vel)
	group_bullet(b)
	return b

func transform_bullet(bullet, pos = null, rot = null, vel = null):
	if pos != null:
		bullet.set_pos(pos)
	else:
		bullet.set_pos(grandparent.get_pos())

	if rot != null:
		bullet.set_rot(rot)
	else:
		bullet.set_rot(grandparent.get_rot())

	if vel != null:
		bullet.set_inherited_vel(vel)
	else:
		bullet.set_inherited_vel(grandparent.get_linear_velocity()) # inherits player/enemy vel

func group_bullet(b): # puts the bullet into the correct group for collision detection
	if grandparent.is_in_group("player"):
		b.add_to_group("player-bullet")
	elif grandparent.is_in_group("enemy"):
		b.add_to_group("enemy-bullet")

func set_shoot_mode(mode):
	heat = 0 # reset heat for new mode
	emit_signal("heat_changed", heat)

	if mode == GLOBALS.ShootMode.NORMAL:
		overheat = 1500
		firing_heat = 450
		cooling_rate = 10
		timer.set_wait_time(0.5)

		ammo = GLOBALS.Ammo.MISSILE
		shoot_mode = GLOBALS.ShootMode.NORMAL

	elif mode == GLOBALS.ShootMode.PLASMA:
		overheat = 1500
		firing_heat = 200
		cooling_rate = 2
		timer.set_wait_time(0.7)

		ammo = GLOBALS.Ammo.PLASMA
		shoot_mode = GLOBALS.ShootMode.PLASMA

	elif mode == GLOBALS.ShootMode.RAPID_FIRE:
		overheat = 5000
		firing_heat = 250
		cooling_rate = 7
		timer.set_wait_time(0.2)

		ammo = GLOBALS.Ammo.LASER_GREEN
		shoot_mode = GLOBALS.ShootMode.RAPID_FIRE

	elif mode == GLOBALS.ShootMode.SCATTER_BEAM:
		overheat = 3000
		firing_heat = 1000
		cooling_rate = 7
		timer.set_wait_time(0.9)

		ammo = GLOBALS.Ammo.LASER_GREEN
		shoot_mode = GLOBALS.ShootMode.SCATTER_BEAM

	else:
		printerr("ERROR: Invalid value passed to gun::set_shoot_mode()")

func on_timer_timeout():
	timer.set_active(false)