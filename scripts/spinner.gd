
extends "res://scripts/enemy.gd"

onready var steer_timer = get_node("steer-timer")
onready var shoot_timer = get_node("shoot-timer")

var can_shoot = true
var shots = 0
var max_shots = 2

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	if ship.hull_armor > 0: # don't spin the explosion animation
		rotate(ship.rot_speed * delta)

		if can_shoot: # don't shoot when dead
			ship.gun.shoot_spinner(get_pos(), get_rot() - PI / 2.6, vel)

func set_level(level):
	.set_level(level)

	# reset shoot mode
	ship.gun.set_shoot_mode(GLOBALS.ShootMode.RAPID_FIRE)
	ship.gun.ammo = GLOBALS.Ammo.LASER_RED # evil red lasers

	if level == GLOBALS.EnemyLevel.LIGHT:
		sprite_ship.set_texture(load("res://images/spinner-light.png"))

		ship.move_speed = 25
		ship.rot_speed = 1
		ship.hull_armor = 2
		steer_timer.set_wait_time(15)
		shoot_timer.set_wait_time(4)

	if level == GLOBALS.EnemyLevel.HEAVY:
		sprite_ship.set_texture(load("res://images/spinner-heavy.png"))

		ship.move_speed = 75
		ship.rot_speed = 1.25
		ship.hull_armor = 3
		steer_timer.set_wait_time(12)
		shoot_timer.set_wait_time(5)

	if level == GLOBALS.EnemyLevel.BOSS:
		sprite_ship.set_texture(load("res://images/spinner-boss.png"))

		ship.move_speed = 125
		ship.rot_speed = 1.5
		steer_timer.set_wait_time(8)
		shoot_timer.set_wait_time(6)
		max_shots = 3

	vel = GLOBALS.rand_vel(ship.move_speed) # reset move speed

func on_steer_timer_timeout():
	if ship.hull_armor > 0: # don't steer if dead
		vel = GLOBALS.rand_vel(ship.move_speed)

func on_shoot_timer_timeout():
	can_shoot = true
	shots = 0

func on_gun_shooting(bullet, ammo):
	.on_gun_shooting(bullet, ammo)
	shots += 1

	if shots >= max_shots:
		can_shoot = false
		shoot_timer.set_active(true)
		shoot_timer.start()