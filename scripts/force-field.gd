
extends Node2D

onready var progress = get_node("progress")
onready var update = get_node("progress/update")

var hp = 0 # number of times force field can be hit before it deactivates

signal activated
signal deactivated

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	set_rot(0 - get_parent().get_rot())

func activate(duration):
	progress.set_max(duration)
	progress.set_value(duration)
	update.start()

	hp = duration
	set_opacity(1)

	progress.show()
	show()
	emit_signal("activated")

func deactivate():
	hp = 0
	hide()
	progress.hide()
	update.stop()
	emit_signal("deactivated")

func damage():
	hp -= 1
	set_opacity(get_opacity() - (1 / progress.get_max()))

	if hp <= 0:
		deactivate()

func on_progress_update_timeout():
	if progress.get_value() > 1:
		progress.set_value(progress.get_value() - 1)
	else:
		deactivate()