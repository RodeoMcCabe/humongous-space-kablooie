const screen_size = Vector2(1000, 750)

static func screenwrap(pos, object): # object must inherit Node2D
	if (pos.x < 0):
		object.set_pos(Vector2(screen_size.x, pos.y))
	elif (pos.x > screen_size.x):
		object.set_pos(Vector2(0, pos.y))
	
	if (pos.y < 0):
		object.set_pos(Vector2(pos.x, screen_size.y))
	elif (pos.y > screen_size.y):
		object.set_pos(Vector2(pos.x, 0))
