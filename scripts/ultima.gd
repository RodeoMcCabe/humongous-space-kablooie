

extends Area2D

const GLOBALS = preload("res://scripts/globals.gd")

func _ready():
	set_pos(GLOBALS.rand_border_point(450))

func on_body_enter(body):
	if body.is_in_group("asteroid") and not body.is_queued_for_deletion():
		body.destroy("ultima")
	elif body.is_in_group("enemy"):
		body.damage("ultima")

func on_anim_finished():
	queue_free()