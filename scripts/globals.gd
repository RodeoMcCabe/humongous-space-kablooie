
const screen_size = Vector2(1000.0, 750.0)

const WHITE = Color("#eeeeec")
const RED = Color("#ff0000")
const ORANGE = Color("#ff7f00")
const YELLOW = Color("#ffff00")
const GREEN = Color("#00bf00")
const BLUE = Color("#003fff")
const PURPLE = Color("#7f00bf")
const GREY = Color("#808080")
const PINK = Color("#ff00ff")

const POINTS_PER_LEVEL = 150

enum Difficulty {
	EASY = 0,
	REAL = 1 }

class GameState:
	const NONE = 0
	const MENU = 1
	const PLAYING = 2
	const PAUSED = 3
	const LOST = 4
	const WON = 5

class Bonus:
	const NONE = 0
	const HULL_ARMOR = 1
	const FORCE_FIELD = 2
	const PLASMA_GUN = 3
	const RAPID_FIRE = 4
	const SCATTER_BEAM = 5
	const ULTIMA = 6
	const SQUISHY_LOVE_ORGAN = 7

class ShootMode:
	const NORMAL = 0
	const PLASMA = 1
	const RAPID_FIRE = 2
	const SCATTER_BEAM = 3

class Ammo:
	const NONE = 0
	const MISSILE = 1
	const PLASMA = 4
	const LASER_GREEN = 2
	const LASER_RED = 3

class EnemyType:
	const DRONE = 0
	const SEEKER = 1
	const SPINNER = 2

class EnemyLevel:
	const LIGHT = 0
	const HEAVY = 1
	const BOSS = 2

##########
########## GLOBAL STATIC FUNCTIONS
##########

# randomly choose a point within inset pixels of the window edge
static func rand_border_point(inset):
	var point = Vector2(0,0)

	var rand_val = randi() % 4

	if rand_val == 0: # top edge
		point.x = randi() % int(screen_size.x)
		point.y = randi() % inset
	elif rand_val == 1: # right edge
		point.x = randi() % inset + (int(screen_size.x) - inset)
		point.y = randi() % int(screen_size.y)
	elif rand_val == 2: # bottom edge
		point.x = randi() % int(screen_size.x)
		point.y = randi() % inset + (int(screen_size.y) - inset)
	elif rand_val == 3: # left edge
		point.x = randi() % inset
		point.y = randi() % int(screen_size.y)
	else:
		printerr("ERROR: invalid randi() value at globals::rand_border_point()")

	return point


static func rand_vel(speed = null, rot = null):

	if rot == null:
		rot = rand_range(0.0, 2 * PI)

	if speed == null:
		speed = rand_range(50, 500)

	return speed * Vector2(sin(rot), cos(rot))