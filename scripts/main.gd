
extends CanvasLayer

const GLOBALS = preload("res://scripts/globals.gd")
const MAIN_MENU = "res://ui/main-menu.scn"
const GAME = "res://game.scn"
const OPTIONS_MENU = "res://ui/options-menu.scn"
const HOW_TO_PLAY = "res://ui/how-to-play.scn"
const CREDITS = "res://ui/credits.scn"

onready var ui = get_node("ui-layer")
onready var bg = get_node("bg-layer")
onready var music = get_node("music")

var current_scene = null
var state = GLOBALS.GameState.MENU
var difficulty = GLOBALS.Difficulty.EASY

func _ready():
	randomize() # seed the rand functions
	main_menu()

func set_scene(scene):
	ui.unpause()
	if (current_scene != null):
		current_scene.queue_free()

	current_scene = load(scene).instance()

	if scene.begins_with("res://ui/"): # put menus/hud on top render layer
		ui.add_child(current_scene)
	else:
		add_child(current_scene)

func main_menu():
	set_scene(MAIN_MENU)
	state = GLOBALS.GameState.MENU

	if not music.is_playing(): # start music
		music.menu()
	elif music.get_stream().get_path() != music.MENU:
		# change to menu music, but don't restart if it's already the menu music
		music.menu()

	ui.hud.hide()
	ui.game_menu.hide()
	current_scene.get_node("buttons/play").grab_focus() # no script for main-menu.scn, so set focus here

	# connect button events
	current_scene.get_node("buttons/play").connect("pressed", self, "start_game")
	current_scene.get_node("buttons/options").connect("pressed", self, "options_menu")
	current_scene.get_node("buttons/how-to-play").connect("pressed", self, "how_to_play")
	current_scene.get_node("buttons/credits").connect("pressed", self, "credits")
	current_scene.get_node("buttons/quit").connect("pressed", self, "quit")

func start_game():
	set_scene(GAME)
	state = GLOBALS.GameState.PLAYING
	current_scene.set_difficulty(difficulty)

func options_menu():
	set_scene(OPTIONS_MENU)
	state = GLOBALS.GameState.MENU
	ui.hud.hide()
	ui.game_menu.hide()
	current_scene.easy_mode.connect("pressed", self, "set_difficulty", [GLOBALS.Difficulty.EASY])
	current_scene.real_mode.connect("pressed", self, "set_difficulty", [GLOBALS.Difficulty.REAL])

func how_to_play():
	set_scene(HOW_TO_PLAY)
	state = GLOBALS.GameState.MENU
	ui.hud.hide()

func credits():
	set_scene(CREDITS)
	state = GLOBALS.GameState.MENU
	ui.hud.hide()

func set_difficulty(difficulty):
	self.difficulty = difficulty

	var game = get_tree().get_nodes_in_group("game") # see ui.gd::pause() for comments
	if game.size() > 0:
		game[0].set_difficulty(difficulty)

func quit():
	get_tree().quit()
