
extends KinematicBody2D

const GLOBALS = preload("res://scripts/globals.gd")
const SCREENWRAP = preload("res://scripts/screenwrap.gd")
const DRONE = "res://objects/drone.scn"
const SEEKER = "res://objects/seeker.scn"
const SPINNER = "res://objects/spinner.scn"

onready var anim = get_node("anim")
onready var sprite_ship = get_node("sprite-ship")
onready var sprite_explosion_small = get_node("sprite-explosion-small")
onready var sprite_explosion_big = get_node("sprite-explosion-big")
onready var ship = get_node("ship")

var vel = Vector2()

signal destroyed
signal gun_shooting

func _ready():
	set_fixed_process(true)

	ship.gun.ammo = GLOBALS.Ammo.LASER_RED # evil red lasers

func _fixed_process(delta):
	SCREENWRAP.screenwrap(get_pos(), self)
	set_pos(get_pos() + vel * delta)

func damage(source):
	if not ship.is_invincible():
		ship.damage(source)

func set_level(level):
	if level == GLOBALS.EnemyLevel.LIGHT:
		add_to_group("light")
		ship.hull_armor = 1
	if level == GLOBALS.EnemyLevel.HEAVY:
		add_to_group("heavy")
		ship.hull_armor = 2
	if level == GLOBALS.EnemyLevel.BOSS:
		add_to_group("boss")
		ship.hull_armor = 5
		ship.gun.set_shoot_mode(GLOBALS.ShootMode.SCATTER_BEAM)
		ship.gun.ammo = GLOBALS.Ammo.LASER_RED # shoot mode sets ammo to green lasers, so set it back

func get_points(source):
	if source == "ultima":
		return 0 # no points if destroyed by ultima

	var points = 10

	if is_in_group("drone"):
		points += 5
	elif is_in_group("spinner"):
		points += 10
	elif is_in_group("seeker"):
		points += 15

	if is_in_group("light"):
		points += 5
	elif is_in_group("heavy"):
		points += 15
	elif is_in_group("boss"):
		points *= 5

	return points

##########
########## CALLBACKS
##########

func on_ship_destroyed(source):
	sprite_ship.hide()
	clear_shapes()

	if is_in_group("boss"):
		sprite_explosion_big.show()
		anim.play("explosion-big")
	else:
		sprite_explosion_small.show()
		anim.play("explosion-small")

	emit_signal("destroyed", get_groups(), get_points(source), get_pos())

func on_gun_shooting(bullet, ammo):
	emit_signal("gun_shooting", bullet, ammo)

func on_anim_finished():
	queue_free()
