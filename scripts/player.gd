
extends RigidBody2D

const GLOBALS = preload("res://scripts/globals.gd")
const SCREENWRAP = preload("res://scripts/screenwrap.gd")
const NONE = 0
const LEFT = 1
const RIGHT = 2

# get collison shapes for changing at runtime
onready var player_collider_shape = get_node("collider").get_polygon()
onready var force_field_collider_shape = get_node("force-field/collider").get_shape()

onready var sprite_accel = get_node("sprite-accelerate")
onready var force_field = get_node("force-field")
onready var ship = get_node("ship")
onready var warp_drive_timer = get_node("warp-drive-timer")
onready var text = get_node("floating-text")
onready var bonus = get_node("bonus")
onready var anim = get_node("anim")

var vel = Vector2(0,0)
var ang_vel = 0.0

var accelerating = false
var turning = NONE
var shooting = false

signal damaged
signal hull_armor_changed
signal gun_heat_changed
signal gun_shooting
signal gun_overheating
signal gun_cooled
signal warped
signal warp_cooldown_changed
signal died

func _ready():
	set_fixed_process(true)
	set_process_input(true)

	warp_drive_timer.set_active(false)
	text.hide()

func _fixed_process(delta):
	SCREENWRAP.screenwrap(get_pos(), self)

	# check for movement
	if accelerating:
		vel = get_linear_velocity() - (ship.move_speed * delta) * Vector2(sin(get_rot()), cos(get_rot()))
		set_linear_velocity(vel)

	if turning == LEFT:
		set_angular_velocity(get_angular_velocity() - ship.rot_speed * delta)
	elif turning == RIGHT:
		set_angular_velocity(get_angular_velocity() + ship.rot_speed * delta)

	if shooting:
		ship.gun.shoot()

	# stop moving if keys aren't pressed anymore
	if not Input.is_action_pressed("ui_up"):
		accelerating = false
		sprite_accel.hide()

	if not Input.is_action_pressed("ui_left") and not Input.is_action_pressed("ui_right"):
		turning = NONE

	if not Input.is_action_pressed("ui_select"):
		shooting = false

	# update warp drive hud
	if warp_drive_timer.is_active(): # if warp drive is on cool down
		emit_signal("warp_cooldown_changed", warp_drive_timer.get_time_left())

func _input(event):
	if event.is_action_pressed("ui_select"):
		shooting = true
	elif event.is_action_pressed("ui_up"):
		accelerating = true
		sprite_accel.show()
		anim.play("accelerating")
	elif event.is_action_pressed("ui_down"):
		warp()
	elif event.is_action_pressed("ui_left"):
		turning = LEFT
	elif event.is_action_pressed("ui_right"):
		turning = RIGHT

func damage(source):
	if force_field.hp > 0:
		force_field.damage()
	elif not ship.is_invincible():
		ship.damage(source)

# move player to a random position at least 100 px from screen border
func warp():
	if not warp_drive_timer.is_active():
		var x = randi() % int(GLOBALS.screen_size.x)
		var y = randi() % int(GLOBALS.screen_size.y)
		var new_pos = Vector2(x, y)
		set_pos(new_pos)

		warp_drive_timer.set_active(true)
		warp_drive_timer.start()

		if force_field.hp <= 0: # make sure force field isn't already active from a pickup
			force_field.activate(5)

		emit_signal("warped")

func set_bonus(new_bonus):
	if new_bonus == GLOBALS.Bonus.NONE:
		ship.gun.set_shoot_mode(GLOBALS.ShootMode.NORMAL)
		text.set_hidden(true)
		bonus.stop()

	elif new_bonus == GLOBALS.Bonus.HULL_ARMOR:
		ship.hull_armor += 1

		text.set_text("HULL ARMOR!")
		text.set_color(GLOBALS.BLUE)
		text.show()
		text.start_timer()

		emit_signal("hull_armor_changed", ship.hull_armor)

	elif new_bonus == GLOBALS.Bonus.RAPID_FIRE :
		ship.gun.set_shoot_mode(GLOBALS.ShootMode.RAPID_FIRE)

		text.set_text("RAPID FIRE!")
		text.set_color(GLOBALS.YELLOW)
		text.show()

		bonus.stop() # override other bonuses that might be active
		bonus.start()

	elif new_bonus == GLOBALS.Bonus.PLASMA_GUN:
		ship.gun.set_shoot_mode(GLOBALS.ShootMode.PLASMA)

		text.set_text("PLASMA GUN!")
		text.set_color(GLOBALS.PURPLE)
		text.set_hidden(false)

		bonus.stop()
		bonus.start()

	elif new_bonus == GLOBALS.Bonus.SCATTER_BEAM:
		ship.gun.set_shoot_mode(GLOBALS.ShootMode.SCATTER_BEAM)

		text.set_text("SCATTER BEAM!")
		text.set_color(GLOBALS.GREEN)
		text.set_hidden(false)

		bonus.stop()
		bonus.start()

	elif new_bonus == GLOBALS.Bonus.FORCE_FIELD:
		force_field.activate(15)

		text.set_text("Force Field!")
		text.set_color(GLOBALS.WHITE)
		text.show()
		text.start_timer()

	elif new_bonus == GLOBALS.Bonus.SQUISHY_LOVE_ORGAN:
		text.set_text("Squishy Alien Love Organ!")
		text.set_color(GLOBALS.PINK)
		text.show()
		text.start_timer()

	else:
		printerr("ERROR: invalid value passed to player::set_bonus()")

##########
########## CALLBACKS
##########

func on_body_enter(body):
	if body.is_in_group("asteroid"):
		damage("asteroid")

func on_ship_damaged(source):
	emit_signal("damaged", get_pos())

func on_ship_hull_armor_changed(hull_armor):
	emit_signal("hull_armor_changed", hull_armor)

func on_ship_destroyed(source):
	# stop explosion animation from colliding or being controlled by the player
	clear_shapes()
	set_process_input(false)
	accelerating = false
	turning = NONE

	sprite_accel.hide()
	get_node("sprite-ship").hide()
	get_node("gun-hud").hide()
	get_node("sprite-explosion").show()
	anim.play("explosion-player")
	anim.connect("finished", self, "on_explosion_finished")
	emit_signal("died")

func on_gun_heat_changed(heat):
	emit_signal("gun_heat_changed", heat)

func on_gun_shooting(bullets, ammo):
	emit_signal("gun_shooting", bullets, ammo)

func on_gun_overheating():
	text.set_text("OVERHEATING")
	text.set_color(GLOBALS.RED)
	text.set_hidden(false)
	emit_signal("gun_overheating")

func on_gun_cooled():
	text.set_hidden(true)
	emit_signal("gun_cooled")

func on_warp_drive_timer_timeout():
	warp_drive_timer.set_active(false)

func on_bonus_timeout():
	set_bonus(GLOBALS.Bonus.NONE)

func on_force_field_activated():
	# set collision detection to the shape of the force field
	clear_shapes()
	add_shape(force_field_collider_shape)

func on_force_field_deactivated():
	# set collision detection to the shape of the player's ship
	clear_shapes()
	var shape = ConvexPolygonShape2D.new()
	shape.set_points(player_collider_shape)
	add_shape(shape)

func on_explosion_finished():
	hide()
