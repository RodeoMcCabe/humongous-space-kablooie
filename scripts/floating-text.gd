
extends Node2D

onready var parent = get_parent()
onready var label = get_node("label")
onready var timer = get_node("timer")

func _ready():
	if parent.is_in_group("player"):
		set_process(true)

func _process(delta): # not called if parent is not the player
	set_rot(0 - parent.get_rot()) # keep text horizontal

func set_text(string):
	label.set_text(string)

func set_color(color):
	label.set("custom_colors/font_color", color)

func start_timer(time = 2.5):
	timer.set_wait_time(time)
	timer.start()

func on_timer_timeout():
	set_hidden(true)
