
extends Node

const GLOBALS = preload("res://scripts/globals.gd")

const ASTEROID = preload("res://objects/asteroid.scn")
const PICKUP = preload("res://objects/pickup.scn")
const DRONE = preload("res://objects/drone.scn")
const SEEKER = preload("res://objects/seeker.scn")
const SPINNER = preload("res://objects/spinner.scn")
const ULTIMA = preload("res://objects/ultima.scn")

onready var ui = get_node("../ui-layer")
onready var music = get_node("../music")
onready var sfx = get_node("../sfx")
onready var player = get_node("player")
onready var asteroid_timer = get_node("asteroid-timer") # used to make asteroids spawn at a minimum once per min
onready var pickup_timer = get_node("pickup-timer")
onready var enemy_timer = get_node("enemy-timer")
onready var combo = get_node("combo-timer")
onready var ultima_timer = get_node("ultima-timer")
onready var chances = get_node("spawn-chances")

var score = 0
var total_asteroids = 0
var level = 1
var next_level = GLOBALS.POINTS_PER_LEVEL
var explosions = 0 # ultima blows up 20 bombs across random spots

var overheat_sfx_voice_id = 0 # for stopping loop when gun cooled

signal score_changed
signal level_up

func _ready():
	music.earth()
	ui.start_game()

	connect("score_changed", ui, "on_game_score_changed")

	player.connect("hull_armor_changed", ui, "on_player_hull_armor_changed")
	player.connect("warped", ui, "on_player_warped")
	player.connect("warp_cooldown_changed", ui, "on_player_warp_cooldown_changed")

func set_difficulty(difficulty):
	if difficulty == GLOBALS.Difficulty.EASY:
		player.ship.move_speed = 350
		player.ship.rot_speed = 30
		player.set_linear_damp(0.25)
		player.set_angular_damp(10)

	elif difficulty == GLOBALS.Difficulty.REAL:
		player.ship.move_speed = 275
		player.ship.rot_speed = 5
		player.set_linear_damp(0)
		player.set_angular_damp(0)

# only used when a new big asteroid is spawned. Asteroids are responsible for spawning
# their own pieces when they are broken.
func spawn_asteroid(vel = null, pos = null):
	if vel == null:
		vel = GLOBALS.rand_vel(50 + level * 10)

	if pos == null:
		pos = GLOBALS.rand_border_point(1)

	var ast = ASTEROID.instance()
	ast.connect("broken", self, "on_asteroid_broken")
	ast.connect("destroyed", self, "on_asteroid_destroyed")
	add_child(ast)

	ast.set_size(3)
	ast.set_linear_velocity(vel)
	ast.set_angular_velocity(rand_range(-7, 7))
	ast.set_pos(pos)

	total_asteroids += 1
	chances.increment_total_objects()

	asteroid_timer.stop()
	asteroid_timer.set_wait_time(30 - level)
	asteroid_timer.start()

func spawn_pickup(bonus, pos = null):
	if pos == null:
		pos = GLOBALS.rand_border_point(150)

	var pickup = PICKUP.instance()
	pickup.connect("picked_up", self, "on_pickup_picked_up")
	add_child(pickup)

	pickup.set_bonus(bonus)
	pickup.set_pos(pos)
	pickup.vel = GLOBALS.rand_vel(20 + 5 * level)

func spawn_enemy(type, level):
	var enemy

	if type == GLOBALS.EnemyType.DRONE:
		enemy = DRONE.instance()
	elif type == GLOBALS.EnemyType.SEEKER:
		enemy = SEEKER.instance()
	elif type == GLOBALS.EnemyType.SPINNER:
		enemy = SPINNER.instance()

	enemy.connect("destroyed", self, "on_enemy_destroyed")
	enemy.connect("gun_shooting", self, "on_gun_shooting")

	add_child(enemy)
	enemy.set_pos(GLOBALS.rand_border_point(1))
	enemy.set_rot(rand_range(0, 2 * PI))
	enemy.set_level(level)

	chances.increment_total_objects()

func score_points(amount, pos = null, is_combo = null):
	score += amount

	if score >= next_level:
		level_up(1)

	emit_signal("score_changed", amount, score, pos, is_combo)

func level_up(num_of_levels):
	level += num_of_levels
	next_level += GLOBALS.POINTS_PER_LEVEL + level * 25
	chances.level_up(level)
	ui.set_main_text("Level " + str(level), GLOBALS.GREEN, 2.5)

	if level == 4:
		boss_battle(GLOBALS.EnemyType.DRONE)
	elif level == 8:
		boss_battle(GLOBALS.EnemyType.SPINNER)
	elif level == 11:
		boss_battle(GLOBALS.EnemyType.SEEKER)

	emit_signal("level_up", level)

func boss_battle(enemy_type):
	spawn_enemy(enemy_type, GLOBALS.EnemyLevel.BOSS)
	enemy_timer.stop() # don't spawn other enemies while battling the boss
	music.boss()
	ui.set_main_text("Boss Battle!", GLOBALS.YELLOW, 2.5)

func boss_defeated(groups):
	ui.set_main_text("Boss Defeated!", GLOBALS.BLUE, 2.5)
	sfx.play("bomb")

	if groups.has("drone"):
		music.boss_defeated(music.MERCURY)
	elif groups.has("spinner"):
		music.boss_defeated(music.VENUS)
	elif groups.has("seeker"):
		music.boss_defeated(music.MARS)
	enemy_timer.start() # restart normal enemy spawning

func ultima():
	var explosion = ULTIMA.instance()
	add_child(explosion)
	explosions += 1
	sfx.play("bomb")

##########
########## CALLBACKS
##########

func on_asteroid_timer_timeout():
	spawn_asteroid()

func on_asteroid_destroyed(points, pos):
	total_asteroids -= 1
	chances.decrement_total_objects()

	if points > 0: # player destroyed it
		combo.increment(pos) # pass pos to keep track of where the latest combo was scored

	if points != 0: # don't play sfx when destroyed by ultima
		sfx.play("asteroid-breaking")

	if total_asteroids < 1:
		spawn_asteroid()

	score_points(points, pos)

func on_asteroid_broken(piece, size): # passed a smaller asteroid that is a piece of the broken one
	piece.connect("destroyed", self, "on_asteroid_destroyed")
	piece.connect("broken", self, "on_asteroid_broken")

	add_child(piece)
	piece.set_size(size)
	total_asteroids += 1

func on_enemy_timer_timeout():
	spawn_enemy(chances.roll_enemy_types(), chances.roll_enemy_levels())
	enemy_timer.set_wait_time(rand_range(15, 30))
	enemy_timer.start()

func on_enemy_destroyed(groups, points, pos):
	chances.decrement_total_objects()

	if groups.has("boss"):
		boss_defeated(groups)
	else:
		sfx.play("explosion")
	score_points(points, pos)

func on_pickup_timer_timeout():
	spawn_pickup(chances.roll_pickups())
	pickup_timer.set_wait_time(rand_range(20, 30))
	pickup_timer.start()

func on_pickup_picked_up(pos, bonus):
	score_points(15, pos)

	if bonus == GLOBALS.Bonus.ULTIMA:
		ultima()
		ultima_timer.start()
		ui.set_main_text("Ultima!", GLOBALS.RED, 5)
	elif bonus == GLOBALS.Bonus.SQUISHY_LOVE_ORGAN:
		sfx.play("squishy-love-organ")
	else:
		sfx.play("pickup")

func on_player_damaged(pos):
	score_points(-10, pos)
	sfx.play("damage")

func on_player_gun_cooled():
	sfx.stop(overheat_sfx_voice_id)

func on_player_gun_overheating():
	score_points(-5, player.get_pos())
	overheat_sfx_voice_id = sfx.play("overheating")

func on_player_warped():
	sfx.play("warp-drive")

func on_player_died():
	sfx.play("explosion")
	ui.game_over(score, level)
	get_parent().state = GLOBALS.GameState.LOST

func on_gun_shooting(bullets, ammo):
	for bullet in bullets:
		bullet.connect("destroyed", self, "on_bullet_destroyed")
		add_child(bullet)
		bullet.set_ammo(ammo) # must call after it's added to the scene

	if ammo == GLOBALS.Ammo.MISSILE:
		sfx.play("bullet-missile")
	elif ammo == GLOBALS.Ammo.LASER_GREEN or ammo == GLOBALS.Ammo.LASER_RED:
		sfx.play("bullet-laser")
	elif ammo == GLOBALS.Ammo.PLASMA:
		sfx.play("bullet-plasma")

func on_bullet_destroyed():
	sfx.play("explosion-small")

func on_combo_scored(combo_chains, pos):
	score_points(combo_chains * 10, pos, true)

	if combo_chains == 6:
		ultima()
		ultima_timer.start()
		ui.set_main_text("Ultima!", GLOBALS.RED, 5)

func on_ultima_timeout():
	if explosions < 20:
		ultima()
	else:
		ultima_timer.stop()
		explosions = 0
