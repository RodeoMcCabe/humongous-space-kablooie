
extends VBoxContainer

const GLOBALS = preload("res://scripts/globals.gd")
const NORMAL = preload("res://images/ui/slider-normal.png")
const FOCUSED = preload("res://images/ui/slider-focused.png")

onready var music = get_node("../../music")
onready var sfx = get_node("../../sfx")

onready var music_slider = get_node("music/vbox/slider")
onready var music_mute = get_node("music/vbox/mute")
onready var sfx_slider = get_node("sfx/vbox/slider")
onready var sfx_mute = get_node("sfx/vbox/mute")
onready var easy_mode = get_node("difficulty/hbox/easy-mode")
onready var real_mode = get_node("difficulty/hbox/real-mode")
onready var back = get_node("back")

onready var music_sb = music_slider.get_stylebox("slider")
onready var sfx_sb = sfx_slider.get_stylebox("slider")

var opening = true # don't call preview_sfx_volume() when menu is first opening

func _ready():
	if music.muted == true:
		music_mute.set_pressed(true)
	if sfx.get_default_volume() == 0:
		sfx_mute.set_pressed(true)

	music_slider.grab_focus()
	music_slider.set_value(music.get_volume() * 50)
	sfx_slider.set_value(sfx.get_default_volume() * 125)

	if get_node("../../").difficulty == GLOBALS.Difficulty.REAL: # gets main
		real_mode.set_pressed(true)
		easy_mode.set_pressed(false)

	opening = false # finished initializing sfx_slider, preview_sfx_volume() can be called now

func preview_sfx_volume():
	if not sfx.is_active() and not opening:
		var num = randi() % 7

		if num == 0:
			sfx.play("explosion")
		elif num == 1:
			sfx.play("warp-drive")
		elif num == 2:
			sfx.play("asteroid-breaking")
		elif num == 3:
			sfx.play("bullet-laser")
		elif num == 4:
			sfx.play("overheating")
		elif num == 5:
			sfx.play("bullet-missile")
		elif num == 6:
			sfx.play("pickup")

##########
########## CALLBACKS
##########

func on_music_slider_changed(value):
	if music_mute.is_pressed() == false:
		music.set_volume(value / 50) # slider range is 1-100 so convert to unit range

func on_sfx_slider_changed(value):
	sfx.set_default_volume(value / 125)

	if sfx_mute.is_pressed() == false:
		preview_sfx_volume()

func on_music_mute_toggled(pressed):
	if pressed:
		music.muted = true
		music.stop()
	else:
		music.muted = false
		music.play(music.get_stream())
		music.set_volume(music_slider.get_value() / 50)

func on_sfx_mute_toggled(pressed):
	if pressed:
		sfx_slider.set_value(0)
	else:
		sfx_slider.set_value(sfx_slider.get_max()) # just put it at max
		preview_sfx_volume()

func on_music_slider_focus_enter():
	music_sb.set_texture(FOCUSED)

func on_music_slider_focus_exit():
	music_sb.set_texture(NORMAL)

func on_sfx_slider_focus_enter():
	sfx_sb.set_texture(FOCUSED)

func on_sfx_slider_focus_exit():
	sfx_sb.set_texture(NORMAL)

func on_back_pressed():
	var main = get_node("../../")
	if main.state == GLOBALS.GameState.MENU:
		main.main_menu()
	elif main.state == GLOBALS.GameState.PAUSED:
		get_parent().pause()
		queue_free()
	elif main.state == GLOBALS.GameState.LOST:
		get_parent().game_over() # default args, puts the game over screen back like it was
		queue_free()