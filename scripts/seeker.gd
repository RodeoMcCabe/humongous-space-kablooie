
extends "res://scripts/enemy.gd"

onready var collider_light = get_node("collider-light").get_polygon()
onready var collider_boss = get_node("collider-boss").get_polygon()

onready var wander_timer = get_node("wander-timer")
onready var shoot_timer = get_node("shoot-timer")
onready var shoot_range = get_node("shoot-range")

var target = null
var target_node_path = ""
var target_rot = 0.0

var state = "wandering"

var can_shoot = false
var shots = 0
var max_shots = 2

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	 # check if target node is assigned but does not exist in scene tree
	if target != null and not get_tree().get_root().has_node(target_node_path):
		target = null # if it doesn't exist, it got freed, so set to null

	if target == null:
		acquire_target()
	else:
		if ship.hull_armor > 0: # don't steer if dead
			steer(delta)

	if can_shoot and ship.hull_armor > 0: # can't shoot when dead
		ship.gun.shoot(get_pos(), get_rot(), vel)
	elif not shoot_timer.is_active():
			check_can_shoot()

	check_target_distance()
	vel = -Vector2(sin(get_rot()), cos(get_rot())) * ship.move_speed

func set_level(level):
	.set_level(level)
	clear_shapes()
	var shape = ConvexPolygonShape2D.new()

	if level == GLOBALS.EnemyLevel.LIGHT:
		sprite_ship.set_texture(load("res://images/seeker-light.png"))
		shape.set_points(collider_light)
		add_shape(shape)

		ship.move_speed = 125
		ship.rot_speed = 1.5

	if level == GLOBALS.EnemyLevel.HEAVY:
		sprite_ship.set_texture(load("res://images/seeker-heavy.png"))
		shape.set_points(collider_light)
		add_shape(shape)

		ship.move_speed = 150
		ship.rot_speed = 1.5
		max_shots = 3


	if level == GLOBALS.EnemyLevel.BOSS:
		sprite_ship.set_texture(load("res://images/seeker-boss.png"))
		shape.set_points(collider_boss)
		add_shape(shape)

		ship.move_speed = 250
		ship.rot_speed = 3.5
		max_shots = 1

func steer(delta):
	target_rot = get_pos().angle_to_point(target.get_pos())
	var rot = get_rot()

	if rot > target_rot:
		if rot - target_rot > 0.01: # otherwise it's close enough
			rotate(-ship.rot_speed * delta)
	else:
		if target_rot - rot > 0.01:
			rotate(ship.rot_speed * delta)

func check_target_distance():
	if get_pos().distance_to(target.get_pos()) < 100: # too close
		if state == "seeking":
			wander() # run away!
		elif state == "wandering":
			wander_timer.stop() # got there before timer ran out
			acquire_target()
	elif get_pos().distance_to(target.get_pos()) > 500: # too far
		acquire_target()

func acquire_target():
	var target_found = false

	for body in shoot_range.get_overlapping_bodies():
		if body.is_in_group("player") and get_pos().distance_to(body.get_pos()) > 150:
			target = body
			target_node_path = target.get_path()
			state = "seeking"
			target_found = true
			break # get out, no more searching

	# didn't find the player, look again for an asteroid
	for body in shoot_range.get_overlapping_bodies():
		if body.is_in_group("asteroid") and get_pos().distance_to(body.get_pos()) > 200:
			target = body
			target_node_path = target.get_path()
			state = "seeking"
			target_found = true
			break

	if not target_found:
		wander()

func wander():
	var t = Node2D.new()
	get_parent().add_child(t)
	t.set_global_pos(GLOBALS.rand_border_point(300))
	target = t
	target_node_path = target.get_path()
	state = "wandering"
	wander_timer.start()

func check_can_shoot():
	if target != null and state == "seeking":
		if get_rot() - target_rot < 0.05 and get_rot() - target_rot > -0.05:
			can_shoot = true
			return

	can_shoot = false

func on_gun_shooting(bullet, ammo):
	.on_gun_shooting(bullet, ammo)
	shots += 1

	if shots >= max_shots:
		can_shoot = false
		shoot_timer.set_active(true)
		shoot_timer.start()

func on_shoot_timer_timeout():
	shoot_timer.set_active(false)
	shots = 0
	check_can_shoot()

func on_wander_timer_timeout():
	acquire_target()
