extends Node2D

const GLOBALS = preload("res://scripts/globals.gd")
const SCREENWRAP = preload("res://scripts/screenwrap.gd")

const MOVE_SPEED = 600 # in pixel per second

onready var bullet = get_node("sprite-bullet")
onready var explosion = get_node("sprite-explosion")
onready var timer = get_node("timer")
onready var anim = get_node("anim")

var vel = Vector2() # in pixels per second

signal destroyed

func _ready():
	set_fixed_process(true)
	clear_shapes() # clear collider so set_ammo() can set it

func _fixed_process(delta):
	SCREENWRAP.screenwrap(get_pos(), self)
	set_pos(get_pos() + vel * delta) # must multiply by delta to get velocity per frame

func set_ammo(ammo):
	if ammo == GLOBALS.Ammo.MISSILE:
		bullet.set_texture(load("res://images/bullet-missile.png"))
		add_shape(get_node("collider-missile").get_shape())
		timer.set_wait_time(1.0)
	elif ammo == GLOBALS.Ammo.PLASMA:
		bullet.set_texture(load("res://images/bullet-plasma.png"))
		add_shape(get_node("collider-plasma").get_shape())
		timer.set_wait_time(2.0)
	elif ammo == GLOBALS.Ammo.LASER_GREEN:
		bullet.set_texture(load("res://images/bullet-laser-green.png"))
		add_shape(get_node("collider-laser").get_shape())
		timer.set_wait_time(1.5)
	elif ammo == GLOBALS.Ammo.LASER_RED:
		bullet.set_texture(load("res://images/bullet-laser-red.png"))
		add_shape(get_node("collider-laser").get_shape())
		timer.set_wait_time(1.0)

func set_inherited_vel(inherited_vel):
	vel = inherited_vel - MOVE_SPEED * Vector2(sin(get_rot()), cos(get_rot()))

func on_body_enter(body):
	if is_in_group("enemy-bullet"): # is this bullet an enemy?
		if body.is_in_group("asteroid") && not body.is_queued_for_deletion():
			body.destroy("enemy-bullet")
			destroy()
		elif body.is_in_group("player"):
			body.damage("enemy-bullet")
			destroy()
	else: # this bullet belongs to the player
		if body.is_in_group("asteroid") && not body.is_queued_for_deletion():
			body.destroy("player-bullet")
			destroy()
		if body.is_in_group("enemy"):
			body.damage("player-bullet")
			destroy()

func destroy():
	clear_shapes() # stop more collisions from happening
	bullet.hide()
	explosion.show()
	anim.play("explosion-bullet")
	vel /= 15
	anim.connect("finished", self, "on_anim_finished")
	emit_signal("destroyed")

func on_anim_finished():
	queue_free()

func on_timer_timeout():
	queue_free()

