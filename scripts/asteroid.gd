
extends RigidBody2D

const GLOBALS = preload("res://scripts/globals.gd")
const SCREENWRAP = preload("res://scripts/screenwrap.gd")

onready var sprite_asteroid = get_node("sprite-asteroid")
onready var sprite_breaking = get_node("sprite-breaking")
onready var big_collider = get_node("collider-big").get_polygon()
onready var mid_collider = get_node("collider-mid").get_polygon()
onready var small_collider = get_node("collider-small").get_polygon()
onready var anim = get_node("anim")

var size = 3

signal destroyed
signal broken

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	SCREENWRAP.screenwrap(get_pos(), self)

func set_size(size):
	clear_shapes()
	var shape = ConvexPolygonShape2D.new()

	# need to scale sprites and set correct size collider
	if size == 3:
		sprite_breaking.scale(Vector2(1.5, 1.5))
		shape.set_points(big_collider)
		set_mass(20)
	elif size == 2:
		sprite_asteroid.scale(Vector2(0.5, 0.5))
		shape.set_points(mid_collider)
		set_mass(10)
	elif size == 1:
		sprite_asteroid.scale(Vector2(0.25, 0.25))
		sprite_breaking.scale(Vector2(0.5, 0.5))
		shape.set_points(small_collider)
		set_mass(5)
	else:
		printerr("ERROR: tried to call asteroid::set_size(size) with invalid parameter.")
		return # don't add collider or set size

	self.size = size
	add_shape(shape)

func destroy(source):
	if size == 3:
		break_asteroid(75.0)
	elif size == 2:
		break_asteroid(20.0)

	clear_shapes()
	set_angular_velocity(0.0)
	set_linear_velocity(get_linear_velocity() * 0.5)

	sprite_asteroid.hide()
	sprite_breaking.show()
	anim.play("breaking")

	emit_signal("destroyed", get_points(source), get_pos())

# break a bigger asteroid into 3 smaller ones
func break_asteroid(impulse_force):
	if size <= 1: # should never get called for this size
		printerr("ERROR: asteroid with size <= 1 called game::break_asteroid()")
		return

	var radians = atan2(-get_linear_velocity().x, -get_linear_velocity().y) # opposite direction of big asteroid's vector
	var impulse_vel = Vector2(sin(radians), cos(radians)) # must define because vel is in pixels/second

	for i in range(3):
		var a = get_parent().ASTEROID.instance() # parent is always game.scn. Prevents preloading asteroid.scn every time a new asteroid is made

		a.set_linear_velocity(get_linear_velocity() * 0.8) # damp inherited velocity of bigger asteroid
		a.set_angular_velocity(rand_range(-7, 7))
		a.set_pos(get_pos() + 5 * impulse_vel) # move asteroid along a little bit to prevent spawning on top of each other
		a.apply_impulse(Vector2(), impulse_force * impulse_vel)

		impulse_vel = impulse_vel.rotated((PI / 3) * 2) # 120 degrees, 1/3 of a full rotation
		emit_signal("broken", a, size - 1)

func get_points(damage_source):
	# score points based on damage source
	if damage_source == "player-bullet":
		if size == 3:
			return 2
		elif size == 2:
			return 4
		else:
			return 6
	elif damage_source == "enemy-bullet":
		if size == 3:
			return -2
		elif size == 2:
			return -4
		else:
			return -6
	elif damage_source == "ultima":
		return 0
	else:
		print("ERROR: invalid value passed to asteroid::get_points()")

func on_anim_finished():
	queue_free()
