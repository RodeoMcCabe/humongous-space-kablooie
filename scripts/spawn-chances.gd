
extends Node

const GLOBALS = preload("res://scripts/globals.gd")

var total_objects = 0
var ultima_boosted = false

var pickups = {
	GLOBALS.Bonus.HULL_ARMOR : 10,
	GLOBALS.Bonus.FORCE_FIELD : 15,
	GLOBALS.Bonus.PLASMA_GUN : 25,
	GLOBALS.Bonus.RAPID_FIRE : 25,
	GLOBALS.Bonus.SCATTER_BEAM : 25,
	GLOBALS.Bonus.ULTIMA : 0,
	GLOBALS.Bonus.SQUISHY_LOVE_ORGAN : 0 }

var enemy_types = {
	GLOBALS.EnemyType.DRONE : 40,
	GLOBALS.EnemyType.SEEKER : 30,
	GLOBALS.EnemyType.SPINNER : 30 }

var enemy_levels = {
	GLOBALS.EnemyLevel.LIGHT : 90,
	GLOBALS.EnemyLevel.HEAVY : 10,
	GLOBALS.EnemyLevel.BOSS : 0 }


func check_roll(percent, rand_num): # percent is the chance the function will return true
	if percent > 99:
		return true
	elif percent > 0:
		if rand_num <= percent: # score!
			return true
		else:
			return false
	else: # percent <= 0
		return false

func roll_pickups():
	var num = (randi() % 100) + 1 # 1 - 100
	var chance = pickups[GLOBALS.Bonus.HULL_ARMOR]

	for i in range(pickups.size()):
		if check_roll(chance, num) == true:
			return i + 1 # add 1 to index to match constants in globals.gd
		chance += pickups[i + 2] # add the next pickup chance

	return 0

func roll_enemy_types():
	var num = (randi() % 100) + 1  # 1 - 100
	var chance = enemy_types[GLOBALS.EnemyType.DRONE]

	for i in range(enemy_types.size()):
		if check_roll(chance, num) == true:
			return i # enemy constants start at 0, so no need to add anything
		chance += enemy_types[i + 1] # add the next chance amount

	return 0

func roll_enemy_levels():
	var num = (randi() % 100) + 1  # 1 - 100
	var chance = enemy_levels[GLOBALS.EnemyLevel.LIGHT]

	for i in range(enemy_levels.size()):
		if check_roll(chance, num) == true:
			return i # enemy constants start at 0, so no need to add anything
		chance += enemy_levels[i + 1]

	return 0

func increment_total_objects():
	total_objects += 1
	check_total_objects()

func decrement_total_objects():
	total_objects -= 1
	check_total_objects()

func check_total_objects():
	if total_objects >= 20 and not ultima_boosted:
		pickups[GLOBALS.Bonus.PLASMA_GUN] -= 2
		pickups[GLOBALS.Bonus.RAPID_FIRE] -= 2
		pickups[GLOBALS.Bonus.SCATTER_BEAM] -= 2
		pickups[GLOBALS.Bonus.ULTIMA] += 6
		ultima_boosted = true

	elif total_objects < 20 and ultima_boosted:
		pickups[GLOBALS.Bonus.PLASMA_GUN] += 2
		pickups[GLOBALS.Bonus.RAPID_FIRE] += 2
		pickups[GLOBALS.Bonus.SCATTER_BEAM] += 2
		pickups[GLOBALS.Bonus.ULTIMA] -= 6
		ultima_boosted = false

func set_low_armor(is_low):
	if is_low:
		pickups[GLOBALS.Bonus.HULL_ARMOR] = 40
		pickups[GLOBALS.Bonus.FORCE_FIELD] = 25
		pickups[GLOBALS.Bonus.PLASMA_GUN] = 15
		pickups[GLOBALS.Bonus.RAPID_FIRE] = 15
		pickups[GLOBALS.Bonus.SCATTER_BEAM] = 15
	else: # back to defaults
		pickups[GLOBALS.Bonus.HULL_ARMOR] = 10
		pickups[GLOBALS.Bonus.FORCE_FIELD] = 15
		pickups[GLOBALS.Bonus.PLASMA_GUN] = 25
		pickups[GLOBALS.Bonus.RAPID_FIRE] = 25
		pickups[GLOBALS.Bonus.SCATTER_BEAM] = 25

func on_player_hull_armor_changed(hull_armor):
	if hull_armor <= 1:
		set_low_armor(true)
	else:
		set_low_armor(false)

func level_up(level):
	if level == 2:
		enemy_types[GLOBALS.EnemyType.DRONE] = 35
		enemy_types[GLOBALS.EnemyType.SEEKER] = 35
		enemy_types[GLOBALS.EnemyType.SPINNER] = 30

		enemy_levels[GLOBALS.EnemyLevel.LIGHT] = 80
		enemy_levels[GLOBALS.EnemyLevel.HEAVY] = 20

	elif level == 3:
		pickups[GLOBALS.Bonus.PLASMA_GUN] = 25
		pickups[GLOBALS.Bonus.RAPID_FIRE] = 25
		pickups[GLOBALS.Bonus.SCATTER_BEAM] = 24
		pickups[GLOBALS.Bonus.ULTIMA] = 1

		enemy_types[GLOBALS.EnemyType.DRONE] = 30
		enemy_types[GLOBALS.EnemyType.SEEKER] = 35
		enemy_types[GLOBALS.EnemyType.SPINNER] = 35

		enemy_levels[GLOBALS.EnemyLevel.LIGHT] = 70
		enemy_levels[GLOBALS.EnemyLevel.HEAVY] = 30

	elif level == 4:

		enemy_types[GLOBALS.EnemyType.DRONE] = 25
		enemy_types[GLOBALS.EnemyType.SEEKER] = 40
		enemy_types[GLOBALS.EnemyType.SPINNER] = 35

		enemy_levels[GLOBALS.EnemyLevel.LIGHT] = 60
		enemy_levels[GLOBALS.EnemyLevel.HEAVY] = 40

	elif level == 5:
		enemy_types[GLOBALS.EnemyType.DRONE] = 20
		enemy_types[GLOBALS.EnemyType.SEEKER] = 45
		enemy_types[GLOBALS.EnemyType.SPINNER] = 35

		enemy_levels[GLOBALS.EnemyLevel.LIGHT] = 50
		enemy_levels[GLOBALS.EnemyLevel.HEAVY] = 50

	elif level == 6:
		pickups[GLOBALS.Bonus.PLASMA_GUN] = 24
		pickups[GLOBALS.Bonus.RAPID_FIRE] = 24
		pickups[GLOBALS.Bonus.SCATTER_BEAM] = 24
		pickups[GLOBALS.Bonus.ULTIMA] = 3

		enemy_types[GLOBALS.EnemyType.DRONE] = 15
		enemy_types[GLOBALS.EnemyType.SEEKER] = 50
		enemy_types[GLOBALS.EnemyType.SPINNER] = 35

		enemy_levels[GLOBALS.EnemyLevel.LIGHT] = 40
		enemy_levels[GLOBALS.EnemyLevel.HEAVY] = 60

	elif level == 7:
		enemy_types[GLOBALS.EnemyType.DRONE] = 10
		enemy_types[GLOBALS.EnemyType.SEEKER] = 50
		enemy_types[GLOBALS.EnemyType.SPINNER] = 40

		enemy_levels[GLOBALS.EnemyLevel.LIGHT] = 30
		enemy_levels[GLOBALS.EnemyLevel.HEAVY] = 70

	elif level == 8:
		enemy_types[GLOBALS.EnemyType.DRONE] = 5
		enemy_types[GLOBALS.EnemyType.SEEKER] = 55
		enemy_types[GLOBALS.EnemyType.SPINNER] = 40

		enemy_levels[GLOBALS.EnemyLevel.LIGHT] = 20
		enemy_levels[GLOBALS.EnemyLevel.HEAVY] = 80

	elif level == 9:
		pickups[GLOBALS.Bonus.PLASMA_GUN] = 23
		pickups[GLOBALS.Bonus.RAPID_FIRE] = 23
		pickups[GLOBALS.Bonus.SCATTER_BEAM] = 23
		pickups[GLOBALS.Bonus.ULTIMA] = 6

		enemy_types[GLOBALS.EnemyType.DRONE] = 0
		enemy_types[GLOBALS.EnemyType.SEEKER] = 60
		enemy_types[GLOBALS.EnemyType.SPINNER] = 40

		enemy_levels[GLOBALS.EnemyLevel.LIGHT] = 10
		enemy_levels[GLOBALS.EnemyLevel.HEAVY] = 90

	elif level == 10:
		pickups[GLOBALS.Bonus.FORCE_FIELD] = 14
		pickups[GLOBALS.Bonus.SQUISHY_LOVE_ORGAN] = 1

		enemy_types[GLOBALS.EnemyType.DRONE] = 0
		enemy_types[GLOBALS.EnemyType.SEEKER] = 70
		enemy_types[GLOBALS.EnemyType.SPINNER] = 30

		enemy_levels[GLOBALS.EnemyLevel.LIGHT] = 0
		enemy_levels[GLOBALS.EnemyLevel.HEAVY] = 100
