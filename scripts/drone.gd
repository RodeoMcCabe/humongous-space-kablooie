
extends "res://scripts/enemy.gd"

const GLOBALS = preload("res://scripts/globals.gd")

onready var collider_light = get_node("collider-light").get_polygon()
onready var collider_heavy = get_node("collider-heavy").get_polygon()
onready var collider_boss = get_node("collider-boss").get_polygon()

onready var steer_timer = get_node("steer-timer")
onready var shoot_timer = get_node("shoot-timer")
onready var shoot_range = get_node("shoot-range")

var target = Vector2()
var steer_direction = "none"

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	if steer_direction == "clockwise":
		rotate(ship.rot_speed * delta)
	elif steer_direction == "counterclockwise":
		rotate(-ship.rot_speed * delta)

	# update vel in sub-scenes because of different behaviors
	if ship.hull_armor > 0: # don't steer if dead
		vel = -Vector2(sin(get_rot()), cos(get_rot())) * ship.move_speed

func set_level(level):
	.set_level(level)
	clear_shapes()
	var shape = ConvexPolygonShape2D.new()

	if level == GLOBALS.EnemyLevel.LIGHT:
		sprite_ship.set_texture(load("res://images/drone-light.png"))
		shape.set_points(collider_light)
		add_shape(shape)

		ship.move_speed = 30
		ship.rot_speed = 0.25

	if level == GLOBALS.EnemyLevel.HEAVY:
		sprite_ship.set_texture(load("res://images/drone-heavy.png"))
		shape.set_points(collider_heavy)
		add_shape(shape)

		ship.move_speed = 40
		ship.rot_speed = 0.25
		shoot_timer.set_wait_time(3.0)

	if level == GLOBALS.EnemyLevel.BOSS:
		sprite_ship.set_texture(load("res://images/drone-boss.png"))
		shape.set_points(collider_boss)
		add_shape(shape)

		ship.move_speed = 60
		ship.rot_speed = 0.25

##########
########## TARGETING AND SHOOTING METHODS
##########

func acquire_target():
	for body in shoot_range.get_overlapping_bodies():
		if body.is_in_group("player"):
			target = body.get_global_pos()
			return # get out, no more searching

	# didn't find the player, look again for an asteroid
	for body in shoot_range.get_overlapping_bodies():
		if body.is_in_group("asteroid"):
			target = body.get_global_pos()
			return

	# no target found
	target = null

func shoot():
	if target != null and ship.hull_armor > 0:
		var angle = get_pos().angle_to_point(target)
		ship.gun.shoot(get_pos(), angle, vel)

##########
########## CALLBACKS
##########

func on_steer_timer_timeout():
	var num = randi() % 3
	if num == 1:
		steer_direction = "clockwise"
	elif num == 2:
		steer_direction = "counterclockwise"
	else: # num == 0
		steer_direction = "none"

	steer_timer.set_wait_time(rand_range(2.0, 5.0))
	steer_timer.start()

func on_shoot_timer_timeout():
	acquire_target()
	shoot()
