
extends Node2D

const GREEN = preload("res://images/ui/gun-hud-green.png")
const YELLOW = preload("res://images/ui/gun-hud-yellow.png")
const RED = preload("res://images/ui/gun-hud-red.png")

onready var parent = get_parent()
onready var gun = get_node("../ship/gun")
onready var progress = get_node("progress")

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	set_rot(0 - parent.get_rot())

	if progress.get_max() != gun.overheat:
		progress.set_max(gun.overheat)

func on_player_gun_heat_changed(heat):
	progress.set_value(heat)

	if progress.get_unit_value() > 0.65 and progress.get_progress_texture() != RED: # not overheating
		progress.set_progress_texture(YELLOW)
	elif progress.get_progress_texture() != RED: # not overheating, but no warning either
		progress.set_progress_texture(GREEN)

func on_player_gun_overheating():
	progress.set_progress_texture(RED)

func on_player_gun_cooled():
	progress.set_progress_texture(GREEN)
