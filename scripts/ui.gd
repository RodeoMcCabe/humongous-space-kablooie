
extends CanvasLayer

const GLOBALS = preload("res://scripts/globals.gd")
const FLOATING_SCORE = preload("res://ui/floating-score.scn")

onready var main = get_parent()

onready var anim = get_node("../anim")

onready var main_text = get_node("main-text")
onready var main_text_timer = get_node("main-text/timer")

onready var game_menu = get_node("game-menu")
onready var menu_score = get_node("game-menu/vbox/score")
onready var menu_level = get_node("game-menu/vbox/level")
onready var menu_message = get_node("game-menu/vbox/message")
onready var menu_continue = get_node("game-menu/continue")
onready var menu_options = get_node("game-menu/options")
onready var menu_main_menu = get_node("game-menu/main-menu")

onready var hud = get_node("hud")
onready var score = get_node("hud/score")
onready var hull_armor = get_node("hud/hull-armor")
onready var warp_drive = get_node("hud/warp-drive")

var level = 1

func _ready():
	set_process_unhandled_input(true)
	main_text_timer.connect("timeout", self, "on_main_text_timer_timeout")
	menu_continue.connect("pressed", self, "unpause")
	menu_options.connect("pressed", self, "options")
	menu_main_menu.connect("pressed", main, "main_menu")

func _unhandled_input(event):
	if event.is_action_pressed("ui_cancel"):
		if main.state == GLOBALS.GameState.PLAYING: # pause game
			pause()

func start_game():
	# reset hud. Using signal callbacks "inappropriately".
	on_game_score_changed(0, 0)
	on_player_hull_armor_changed(3)
	on_player_warp_cooldown_changed(0.00)

	# make sure continue button is connected to the right thing
	# and disconnected from other things so it won't call unnecessary code
	if menu_continue.is_connected("pressed", main, "start_game"):
		menu_continue.disconnect("pressed", main, "start_game")
		menu_continue.connect("pressed", self, "unpause")

	game_menu.hide()
	hud.show()
	set_main_text("Level 1", GLOBALS.GREEN, 2.5)

func pause():
	get_tree().set_pause(true)
	main.state = GLOBALS.GameState.PAUSED
	hud.hide()

	set_main_text("Pause", GLOBALS.BLUE)
	menu_score.set_text(score.get_text())

	# There is always only ever one game scene at a time. Restarting a game deletes the previous
	# game scene and makes a new one. The new scene will always end up at index 0.
	# get_node("../game") doesn't work because new game scenes might be named "game1" or something.
	var level = get_tree().get_nodes_in_group("game")[0].level
	menu_level.set_text("Level = " + str(level))

	menu_continue.set_text("Continue")
	menu_continue.grab_focus()

	game_menu.show() # must set parent visibility before children's
	menu_message.hide()

	if randi() % 1000 == 0:
		menu_message.set_text("Your butt smells. Clean yourself, you filthy animal.")
		menu_message.show()

func unpause():
	get_tree().set_pause(false)
	main.state = GLOBALS.GameState.PLAYING
	main_text.hide()
	game_menu.hide()
	hud.show()

func options():
	main_text.hide()
	hud.hide()
	game_menu.hide()

	# don't use main.gd::set_scene() because it will delete the current game.gd instance
	var options = load("res://ui/options-menu.scn").instance()
	add_child(options)
	options.easy_mode.connect("pressed", main, "set_difficulty", [GLOBALS.Difficulty.EASY])
	options.real_mode.connect("pressed", main, "set_difficulty", [GLOBALS.Difficulty.REAL])

func game_over(score = null, level = null):
	hud.hide()

	if score != null:
		menu_score.set_text("Score = " + str(score))
	if level != null:
		menu_level.set_text("Level = " + str(level))

		if level >= 20:
			menu_message.set_text("Get a life.")
		elif level >= 11:
			menu_message.set_text("You're a Master Blaster! You can go home now.")
		elif level >= 8:
			menu_message.set_text("Hey, you're pretty good.")
		elif level >= 6:
			menu_message.set_text("Have some praise, I guess.")
		elif level == 5:
			menu_message.set_text("Not bad.")
		elif level == 4:
			menu_message.set_text("Well, you're learning.")
		elif level == 3:
			menu_message.set_text("Is that it?")
		elif level == 2:
			menu_message.set_text("You suck!")
		elif level == 1:
			menu_message.set_text("Level 1!? ULTIMATE FAILURE.")

	# make sure continue button is connected to the right thing
	# and disconnected from other things so it won't call unnecessary code
	if menu_continue.is_connected("pressed", self, "unpause"):
		menu_continue.disconnect("pressed", self, "unpause")
		menu_continue.connect("pressed", main, "start_game")

	menu_continue.set_text("Play Again")
	menu_continue.grab_focus()

	set_main_text("Game over!", GLOBALS.RED)
	game_menu.show()
	menu_message.show() # might still be hidden from pausing

func set_main_text(text, color = null, timeout = null):
	main_text.set_text(text)

	if color != null:
		main_text.set("custom_colors/font_color", color)

	if text.to_lower() == "ultima!":
		anim.play("ultima")

	main_text.show()

	if timeout != null:
		main_text_timer.set_wait_time(timeout)
		main_text_timer.start()
	else:
		main_text_timer.stop() # in case it's still running

##########
########## CALLBACKS
##########

func on_game_score_changed(change, total, pos = null, is_combo = null):
	self.score.set_text("Score = " + str(total))

	if total < 0:
		score.set("custom_colors/font_color", GLOBALS.RED)
	else:
		score.set("custom_colors/font_color", GLOBALS.BLUE)

	if pos != null:
		var fscore = FLOATING_SCORE.instance()
		fscore.set_pos(pos)
		add_child(fscore)
		# must add to scene before using setters, so fscore's _ready() is called
		fscore.set_value(change, is_combo)

func on_player_hull_armor_changed(hull_armor):
	self.hull_armor.set_text("Hull Armor = " + str(hull_armor))

	if hull_armor <= 1:
		self.hull_armor.set("custom_colors/font_color", GLOBALS.RED)
	else:
		self.hull_armor.set("custom_colors/font_color", GLOBALS.BLUE)

func on_player_warped():
	warp_drive.set("custom_colors/font_color", GLOBALS.RED)

func on_player_warp_cooldown_changed(cooldown):
	if cooldown >= 0.02: # less than that is close enough to zero
		warp_drive.set_text("Warp Drive = " + str(cooldown))
		cooldown = stepify(cooldown, 0.01) # round to nearest hundredth

		# make sure trailing zeros are printed
		if cooldown - floor(cooldown) <= 0.009:
			warp_drive.set_text("Warp Drive = " + str(cooldown) + ".00")
		else:
			warp_drive.set_text("Warp Drive = " + str(cooldown))
	else: # cooldown is effectively zero
		warp_drive.set_text("Warp Drive = 0.00")
		warp_drive.set("custom_colors/font_color", GLOBALS.BLUE)

func on_main_text_timer_timeout():
	main_text.hide()
	if anim.is_playing():
		anim.stop()
