
extends Area2D

const GLOBALS = preload("res://scripts/globals.gd")
const SCREENWRAP = preload("res://scripts/screenwrap.gd")

const ARMOR = "res://images/pickup-armor.png"
const FORCE_FIELD = "res://images/pickup-forcefield.png"
const RAPID_FIRE = "res://images/pickup-rapidfire.png"
const PLASMA_GUN = "res://images/pickup-plasmagun.png"
const SCATTER_BEAM = "res://images/pickup-scatterbeam.png"
const ULTIMA = "res://images/pickup-ultima.png"
const SQUISHY_LOVE_ORGAN = "res://images/pickup-squishy-love-organ.png"

onready var sprite = get_node("sprite")

var bonus = 0
var vel = Vector2()

signal picked_up

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	SCREENWRAP.screenwrap(get_pos(), self)
	set_pos(get_pos() + vel * delta)

func set_bonus(bonus):
	if bonus == GLOBALS.Bonus.HULL_ARMOR:
		self.bonus = bonus
		sprite.set_texture(load(ARMOR))
	elif bonus == GLOBALS.Bonus.RAPID_FIRE:
		self.bonus = bonus
		sprite.set_texture(load(RAPID_FIRE))
	elif bonus == GLOBALS.Bonus.PLASMA_GUN:
		self.bonus = bonus
		sprite.set_texture(load(PLASMA_GUN))
	elif bonus == GLOBALS.Bonus.SCATTER_BEAM:
		self.bonus = bonus
		sprite.set_texture(load(SCATTER_BEAM))
	elif bonus == GLOBALS.Bonus.FORCE_FIELD:
		self.bonus = bonus
		sprite.set_texture(load(FORCE_FIELD))
	elif bonus == GLOBALS.Bonus.ULTIMA:
		self.bonus = bonus
		sprite.set_texture(load(ULTIMA))
	elif bonus == GLOBALS.Bonus.SQUISHY_LOVE_ORGAN:
		self.bonus = bonus
		sprite.set_texture(load(SQUISHY_LOVE_ORGAN))
	else:
		printerr("ERROR: invalid value passed to pickup::set_bonus()")

func on_body_enter(body):
	if body.is_in_group("player"):
		body.set_bonus(bonus)
		emit_signal("picked_up", get_pos(), bonus)
		queue_free()