
extends Timer

const SCORE = preload("res://ui/floating-score.scn")

export var base_wait_time = 3
export var chain_wait_time = 2

var hits = 0
var chains = 1 # after scoring one combo, further combos can be chained for more points

signal scored

func _ready():
	set_wait_time(base_wait_time)

func increment(pos):
	if hits == 0:
		start() # start the combo timer

	hits += 1

	check_for_combo(pos)

func check_for_combo(pos):
	if chains == 1:
		if hits >= 3:
			score_combo(pos)
	else:
		if hits >= 2:
			score_combo(pos)

func score_combo(pos):
	# reset timer
	stop()
	if chains == 1:
		set_wait_time(base_wait_time)
	else:
		set_wait_time(chain_wait_time)
	start()

	emit_signal("scored", chains, pos)
	hits = 0

	if chains < 6: # 6 is highest combo chain
		chains += 1 # increment after signal
	else:
		chains = 1 # reset if 6 chains were reached
		set_wait_time(base_wait_time)

func on_combo_timeout():
	hits = 0
	chains = 1
