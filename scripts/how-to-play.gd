
extends VBoxContainer

onready var scroll = get_node("body").get_v_scroll()

var direction = "none"

func _ready():
	set_process_input(true)
	set_process(true)

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_node("../../").main_menu()
	elif event.is_action_pressed("ui_up"):
		direction = "up"
	elif event.is_action_pressed("ui_down"):
		direction = "down"
	elif not event.is_echo():
		direction = "none"

func _process(delta):
	if direction == "up":
		scroll.set_value(scroll.get_value() - 5)
	elif direction == "down":
		scroll.set_value(scroll.get_value() + 5)